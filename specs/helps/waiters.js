class CustomWaiters {

    forSpinner() {
        const spinner = $('div#preloader');
        spinner.waitForDisplayed(10000);
        spinner.waitForDisplayed(10000, true);
    };

    forNotificationDisappear () {
        const notification = $('div.toast div');
        notification.waitForDisplayed(5000, true);
    };
};

module.exports = CustomWaiters;