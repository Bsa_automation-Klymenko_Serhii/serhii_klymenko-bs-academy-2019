const credentials = require('./../../TestData.json');
const LoginActions = require('./../actions/login/login_pa');
const MenuActions = require('./../actions/menu_pa');
const PlaceActions = require('./../actions/place/new_place_pa');
const SignInActions = require('./../actions/signin/signin_pa');
const CustomWaiters = require('./waiters');
const ListsActions = require('./../actions/lists/Lists_pa');
const PlacePageActions = require('./../actions/place/place_page_pa');

const placeSteps = new PlaceActions();
const LoginSteps = new LoginActions();
const MenuSteps = new MenuActions();
const SignInSteps = new SignInActions();
const wait = new CustomWaiters();
const ListPageSteps = new ListsActions();
const placepage = new PlacePageActions();

class PageHelpers {

    maximizeApp() {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);
    };

    userLogin(mail, pwd) {
        LoginSteps.enterEmail(mail);
        LoginSteps.enterPwd(pwd);
        LoginSteps.confirmLogin();
    };

    openFirstPlace() {
        const firstPlace = $('//*[@id="app"]/section/section[1]/div[4]/div[1]/div[1]/div[1]/h3/a');
        firstPlace.waitForDisplayed(2000, 'places dont displayed, something going wrong');

            if (firstPlace.isDisplayed() == true) {
        firstPlace.click();
            } else {
                browser.url('https://165.227.137.250/places/486');
            };
    
    };

    getNotificationText() {
        const notificationMessage = $('div.toast.is-top');
        notificationMessage.waitForDisplayed(10000);
        return notificationMessage.getText();
       };  


    fillNewPlaceForm() {
        placeSteps.enterPlaceName(credentials.placeName);
        placeSteps.choosePlaceCity(credentials.cityName);
        placeSteps.enterPlaceZip(credentials.placeZip);
        placeSteps.enterPlaceAdress(credentials.placeAdress);
        placeSteps.enterPlacePhone(credentials.placePhone);
        placeSteps.enterPlaceSite(credentials.placeSite);
        placeSteps.enterPlaceDesc(credentials.placeDesc);
    };

    fillSignUpForm() {
        SignInSteps.enterName(credentials.name);
        SignInSteps.enterSurname(credentials.surname);
        SignInSteps.enterEmail(credentials.newEmail);
        SignInSteps.enterPwd(credentials.pwd);
    };

    openLastList() {
        MenuSteps.navigateToLists();
        //wait.forSpinner();
        ListPageSteps.openList();
    };

};

module.exports = PageHelpers;