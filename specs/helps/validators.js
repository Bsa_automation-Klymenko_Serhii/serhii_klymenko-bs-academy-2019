const assert = require('assert');
const PlacePageActions = require('./../actions/place/place_page_pa');
const PageHelpers = require('./helpers');
const MenuActions = require('./../actions/menu_pa');
const CustomWaiters = require('./waiters');

const placepage = new PlacePageActions();
const help = new PageHelpers();
const wait = new CustomWaiters();
const MenuSteps = new MenuActions();

class CustomVaidators {

        tagTextValidator() {
            assert.equal(placepage.tagSelect.getText(), 'Bar');
        }

        categoryTextValidator() {
            assert.equal(placepage.categorySelect.getText(), 'Milk bar');
        };

        lastVisitedPlaceName() {
            const listPlaceName = $('//*[@id="app"]/section/section[1]/section/div[1]/div/div[1]/h3/a');

            const placeSavedName = placepage.getPlaceName();
            MenuSteps.navigateToVIsited();
            wait.forSpinner();
            listPlaceName.waitForDisplayed(3000);
            const lastPlaceNameValue = listPlaceName.getText();

            assert.equal(placeSavedName, lastPlaceNameValue);

        };

        listNotEmpty() {
            const lastPlaceInList = $('//*[@id="app"]/div[2]/div/div[1]/div/div[2]/div[1]/div[1]/h3/a');
            browser.pause(2000);
            assert.equal(lastPlaceInList.isDisplayed(), true);
        };

        checkInAndCount() {
            const countBefore = placepage.checkInCount();
            placepage.checkIn();
            browser.pause(1500);
            const countAfter = placepage.checkInCount();

            assert.notEqual(countBefore, countAfter);
        };

};

module.exports = CustomVaidators;