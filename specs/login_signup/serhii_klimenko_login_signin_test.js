const SignInActions = require('./actions/signin/signin_pa');
const LoginActions = require('./actions/login/login_pa');
const PageHelpers = require('./helps/helpers');
const CustomWaiters = require('./helps/waiters');

const assert = require('assert');

const credentials = require('./../TestData.json');

const SignInSteps = new SignInActions();
const LoginSteps = new LoginActions();
const help = new PageHelpers();
const wait = new CustomWaiters();


describe('login & signIn', () => {

    beforeEach(() => {
        help.maximizeApp();
    })

    afterEach(() => {
        browser.reloadSession();
    });

    it('shouldnt have login', () => {

        help.userLogin(credentials.defaultEmail, credentials.incorrectPwd);
        assert.equal(help.getNotificationText(), credentials.incorrectLogin);

    });

    it('should have sing in', () => {
          LoginSteps.movetoSignIn();

          help.fillSignUpForm();
          SignInSteps.confirmSignin();

          assert.equal(help.getNotificationText(), credentials.successSignIn);
                    
     });

    });