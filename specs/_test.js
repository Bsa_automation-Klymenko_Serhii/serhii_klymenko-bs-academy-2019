const SignInActions = require('./actions/signin/signin_pa');
const ListsActions = require('./actions/lists/Lists_pa');
const NewListAction = require('./actions/lists/new_list_pa');
const MenuActions = require('./actions/menu_pa');
const PlaceActions = require('./actions/place/new_place_pa');
const LoginActions = require('./actions/login/login_pa');
const PlacePageActions = require('./actions/place/place_page_pa');
const PageHelpers = require('./helps/helpers');
const CustomVaidators = require('./helps/validators');
const CustomWaiters = require('./helps/waiters');



const assert = require('assert');
const path = require('path');

const credentials = require('./../TestData.json');

const SignInSteps = new SignInActions();
const ListPageSteps = new ListsActions();
const NewListSteps = new NewListAction();
const MenuSteps = new MenuActions();
const placeSteps = new PlaceActions();
const LoginSteps = new LoginActions();
const placepage = new PlacePageActions();
const help = new PageHelpers();
const validate = new CustomVaidators();
const wait = new CustomWaiters();

describe('login & signIn', () => {

    beforeEach(() => {
        help.maximizeApp();
    })

    afterEach(() => {
        browser.reloadSession();
    });

    it('shouldnt have login', () => {

        help.userLogin(credentials.defaultEmail, credentials.incorrectPwd);
        assert.equal(help.getNotificationText(), credentials.incorrectLogin);

    });

    it('should have sing in', () => {
          LoginSteps.movetoSignIn();

          help.fillSignUpForm();
          SignInSteps.confirmSignin();

          assert.equal(help.getNotificationText(), credentials.successSignIn);
                    
     });

    });

    describe('Lists', () => {

     beforeEach(() => {
        help.maximizeApp();
        help.userLogin(credentials.defaultEmail, credentials.pwd);
        wait.forSpinner();
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should create list ', () => {

        MenuSteps.navigateToLists();
        wait.forSpinner();

        ListPageSteps.createList();
        NewListSteps.enterListName(credentials.listName);
        NewListSteps.saveList();
        wait.forSpinner();

        assert.equal(ListPageSteps.getLastListName(), credentials.listName);

    });

    it('should add place to list', () =>{
        help.openFirstPlace();
        wait.forSpinner();
        placepage.savePlaceInList();
        MenuSteps.navigateToLists();
        //wait.forSpinner();
        help.openLastList();
        validate.listNotEmpty();

    });

    it('should delete list', () => {

        MenuSteps.navigateToLists();
        wait.forSpinner();

        ListPageSteps.deleteLastList();

        assert.equal(ListPageSteps.noListsCheck(), true, 'Delete all lists, expect 1 and repeat test');
        
    });


});

    describe('Places', () => {

        beforeEach(() => {
           help.maximizeApp();
           help.userLogin(credentials.defaultEmail, credentials.pwd);
           wait.forSpinner();
       });
   
       afterEach(() => {
           browser.reloadSession();
       });

    it('should add new place', () => {

        MenuSteps.navigateToNewPlace();

        help.fillNewPlaceForm();

        placeSteps.moveToNextStep(0);
        placeSteps.uploadPicture(path.join(__dirname, credentials.picPath));
        placeSteps.moveToNextStep(1);
        placeSteps.moveToNextStep(2);
        placeSteps.categorySelect();
        placeSteps.tagSelect();
        placeSteps.moveToNextStep(3);
        placeSteps.featureSwitch();
        placeSteps.moveToNextStep(4);
        placeSteps.moveToNextStep(5);
        placeSteps.moveToNextStep(6);
        wait.forSpinner();
        
        assert.equal(placepage.getPlaceName(), credentials.placeName);

    });

    it('should leave comment', () => {

        help.openFirstPlace();
        wait.forSpinner();
        placepage.leaveComment(credentials.commentText);

        assert.equal(placepage.getLastCommentText(), credentials.commentText);

    });

    it('should cheked in', () => {

        help.openFirstPlace();
        wait.forSpinner();

        validate.checkInAndCount();

        assert.equal(help.getNotificationText(), credentials.successCheckIn);
        wait.forNotificationDisappear();
        
        validate.lastVisitedPlaceName();

    });

});