const PageHelpers = require('./helps/helpers');
const CustomVaidators = require('./helps/validators');
const CustomWaiters = require('./helps/waiters');
const ListsActions = require('./actions/lists/Lists_pa');
const NewListAction = require('./actions/lists/new_list_pa');
const MenuActions = require('./actions/menu_pa');

const assert = require('assert');

const credentials = require('./../TestData.json');

const ListPageSteps = new ListsActions();
const NewListSteps = new NewListAction();
const help = new PageHelpers();
const validate = new CustomVaidators();
const wait = new CustomWaiters();
const MenuSteps = new MenuActions();

describe('lists suite', () => {

    beforeEach(() => {
        help.maximizeApp();
        help.userLogin(credentials.defaultEmail, credentials.pwd);
        wait.forSpinner();
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should create list ', () => {

        MenuSteps.navigateToLists();
        wait.forSpinner();

        ListPageSteps.createList();
        NewListSteps.enterListName(credentials.listName);
        NewListSteps.saveList();
        wait.forSpinner();

        assert.equal(ListPageSteps.getLastListName(), credentials.listName);

    });

    it('should add place to list', () =>{
        help.openFirstPlace();
        wait.forSpinner();
        placepage.savePlaceInList();
        MenuSteps.navigateToLists();
        //wait.forSpinner();
        help.openLastList();
        validate.listNotEmpty();

    });

    it('should delete list', () => {

        MenuSteps.navigateToLists();
        wait.forSpinner();

        ListPageSteps.deleteLastList();

        assert.equal(ListPageSteps.noListsCheck(), true, 'Delete all lists, expect 1 and repeat test');
        
    });

});