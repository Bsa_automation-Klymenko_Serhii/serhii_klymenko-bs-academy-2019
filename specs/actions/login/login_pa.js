const LoginPage = require('./../../pages/login/login_po');
const logpage = new LoginPage();

class LoginActions {

    enterEmail(value) {
        logpage.mailField.waitForDisplayed(2000);
        logpage.mailField.clearValue();
        logpage.mailField.setValue(value);
    };

    enterPwd(value) {
        logpage.pwdField.waitForDisplayed(2000);
        logpage.pwdField.clearValue();
        logpage.pwdField.setValue(value);
     };

     confirmLogin() {
        logpage.loginBtn.waitForDisplayed(2000);
        logpage.loginBtn.click();
    };

    movetoSignIn() {
        logpage.signInBtn.waitForDisplayed(2000);
        logpage.signInBtn.click();
    };

};

module.exports = LoginActions;