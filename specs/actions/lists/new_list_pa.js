const NewList = require('./../../pages/lists/new_list_po');
const newlistpage = new NewList();

class NewListActions {
    enterListName(value){

        newlistpage.listNameInput.waitForDisplayed(5000);
        newlistpage.listNameInput.setValue(value);
    };

    saveList() {
        newlistpage.saveListBtn.click();
    };

};

module.exports = NewListActions;