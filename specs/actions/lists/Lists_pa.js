const ListsPage = require('./../../pages/lists/list_po');
const listpage = new ListsPage();

class ListsActions {

    createList() {
        listpage.createListBtn.waitForDisplayed(2000);
        listpage.createListBtn.click();
    };

    getLastListName() {
        listpage.lastlistName.waitForDisplayed(2000);
        return listpage.lastlistName.getText();
    };

    deleteLastList() {

        listpage.deleteListBtn.waitForDisplayed(2000);
        listpage.deleteListBtn.click();
        listpage.deleteConfirmBtn.waitForDisplayed(2000);
        listpage.deleteConfirmBtn.click();
    };

    noListsCheck() {
        listpage.emptyLists.waitForDisplayed(2000)
        return listpage.emptyLists.isDisplayed();
    };

    openList() {
        listpage.lastlistName.waitForDisplayed(2000);
        listpage.lastlistName.click();
    }

};

module.exports = ListsActions;