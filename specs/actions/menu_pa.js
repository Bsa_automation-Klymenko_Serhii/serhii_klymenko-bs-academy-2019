const MenuPage = require('./../pages/menu_po');
const menu = new MenuPage();

class MenuActions {

    _moveToMenu() {
        menu.dropdown.waitForDisplayed(10000);
        menu.dropdown.moveTo();
    };

    navigateToTastes() {
        this._moveToMenu();
        menu.tastes.waitForDisplayed(2000);
        menu.tastes.click();
    };

    navigateToLists() {
        this._moveToMenu();
        menu.lists.waitForDisplayed(2000);
        menu.lists.click();
    };

    navigateToVIsited() {
        this._moveToMenu();
        menu.visited.waitForDisplayed(2000);
        menu.visited.click();
    };

    navigateToNewPlace() {
        this._moveToMenu();
        menu.newPlace.waitForDisplayed(2000);
        menu.newPlace.click();
    };

    navigateToSettings() {
        this._moveToMenu();
        menu.settings.waitForDisplayed(2000);
        menu.settings.click();
    };

    navigateToNOtifications() {
        this._moveToMenu();
        menu.notification.waitForDisplayed(2000);
        menu.notification.click();
    };

    navigateToTastes() {
        this._moveToMenu();
        menu.logOut.waitForDisplayed(2000);
        menu.logOut.click();
    };

};

module.exports = MenuActions;