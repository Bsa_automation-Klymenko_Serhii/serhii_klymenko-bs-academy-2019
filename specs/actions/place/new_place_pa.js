const NewPlacePage = require('./../../pages/place/new_place_po');
const placepage = new NewPlacePage();

class NewPlaceAction {

        enterPlaceName(value) {
            placepage.placeName.waitForDisplayed(2000);
            placepage.placeName.setValue(value);
        };

        choosePlaceCity(value) {
            placepage.placeCity.clearValue();
            placepage.placeCity.setValue(value);
            placepage.cityDropDown.waitForDisplayed(3000);
            placepage.cityDropDown.click();      
        };

        enterPlaceZip(value) {
            placepage.placeZip.setValue(value);
        };

        enterPlacePhone(value) {
            placepage.placePhone.setValue(value);
        };

        enterPlaceAdress(value) {
            placepage.placeAdress.setValue(value);
        };

        enterPlaceSite(value) {
            placepage.placeSite.setValue(value);
        };

        enterPlaceDesc(value) {
            placepage.placeDesc.setValue(value);
        };

        uploadPicture(path) {
            browser.pause(2000);
            placepage.pictureUpload.setValue(path);
            placepage.imagePreview.waitForDisplayed(5000);
        };

        categorySelect() {
            browser.pause(1000);
            placepage.categorySelect.selectByIndex(1);
        };

        tagSelect() {
            browser.pause(1000);
            placepage.tagSelect.selectByIndex(1);
        };

        featureSwitch() {
            browser.pause(1000);
            placepage.featureSelect.waitForDisplayed(5000);
            placepage.featureSelect.click();
        };

        moveToNextStep(x) {
            browser.pause(1000);
            placepage.nextStepBtn[x].click();
        };   
};

module.exports = NewPlaceAction;