const PlacePage = require('./../../pages/place/place_page_po');
const placeInfoPage = new PlacePage();

class PlacePageActions {

 leaveComment(text) {
    placeInfoPage.placeCommentField.waitForDisplayed(3000, 'Comment field doesnt displayed');
    placeInfoPage.placeCommentField.setValue(text);
    placeInfoPage.postCommentBtn.click();
    browser.pause(1500);
};

getLastCommentText() {
    return placeInfoPage.lastCommentText.getText();
};

checkIn() {
    placeInfoPage.checkInBtn.waitForDisplayed(3000);
    placeInfoPage.checkInBtn.click();
};

savePlaceInList (){
    placeInfoPage.saveBtn.waitForDisplayed(3000);
    placeInfoPage.saveBtn.click();
    placeInfoPage.saveDropDown.waitForDisplayed(3000);
    placeInfoPage.saveDropDown.click();
};

getPlaceName() {
    placeInfoPage.savedPlaceName.waitForDisplayed(10000);
    return placeInfoPage.savedPlaceName.getText();
};

checkInCount() {
    placeInfoPage.checkInBtn.waitForDisplayed(3000);
    return placeInfoPage.checkInBtn.getText();
}

};

module.exports = PlacePageActions;