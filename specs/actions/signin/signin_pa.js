const SignInPage = require('./../../pages/signin/signin_po');
const signpage = new SignInPage();

class SignInActions {

    enterName(value) {
        signpage.nameInput.waitForDisplayed(2000);
        signpage.nameInput.setValue(value);
    };

    enterSurname(value) {
        signpage.surnameInput.waitForDisplayed(2000);
        signpage.surnameInput.setValue(value);
    };

    enterEmail(value) {
        signpage.emailInput.waitForDisplayed(2000);
        signpage.emailInput.setValue(value);
    };

    enterPwd(value) {
       signpage.okCheckbox.waitForDisplayed(2000);
       signpage.passwordInput.waitForDisplayed(2000);
       signpage.passwordInput.setValue(value);
    };

    confirmSignin() {
        signpage.loginBtn.waitForDisplayed(2000);
        signpage.loginBtn.click();
    };
};

module.exports = SignInActions;