class NewList {
    get listNameInput () {return $('input#list-name')};
    get saveListBtn () {return $('button.button.is-success')};
};

module.exports = NewList;