class ListsPage {
    get createListBtn () {return $('a[href="/my-lists/add"]')};
    get lastlistName () {return $('//*[@id="app"]/section/ul/li[1]/div/div/div[1]/h3/a')};
    get deleteListBtn () {return $('button.button.is-danger')};
    get deleteConfirmBtn () {return $('div.buttons button.is-danger')};
    get emptyLists () {return $('div.no-lists-text')};

};

module.exports = ListsPage;