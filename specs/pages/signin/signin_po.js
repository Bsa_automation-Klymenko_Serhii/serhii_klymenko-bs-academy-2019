class SignInPage {
    get emailInput () {return $('input[name="email"]')};
    get passwordInput () {return $('input[type="password"]')};
    get nameInput () {return $('input[name="firstName"]')};
    get surnameInput () {return $('input[name="lastName"]')};
    get loginBtn () {return $('button.button.is-primary')};
    get okCheckbox () {return $('span.icon.is-right.has-text-success')};
};

module.exports = SignInPage;