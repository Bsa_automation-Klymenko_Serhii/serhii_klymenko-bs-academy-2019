class MenuPage {
    get dropdown () {return $('div.profile')};
    get tastes () {return $('a[href="/my-tastes"]')};
    get lists () {return $('a[href="/my-lists"]')};
    get visited () {return $('a[href="/checkins"]')};
    get newPlace () {return $('a[href="/places/add"]')};
    get settings () {return $('a[href="/settings"]')};
    get notification () {return $('a[href="/notification"]')};
    get logOut () {return $('a*=log out')};
};

module.exports = MenuPage;