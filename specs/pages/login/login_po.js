class LoginPage {

    get mailField () {return $('input[name="email"]')};
    get pwdField () {return $('input[type="password"]')};
    get loginBtn () {return $('button.button.is-primary')};
    get signInBtn () {return $('a.link.link-signup')}

};

module.exports = LoginPage;