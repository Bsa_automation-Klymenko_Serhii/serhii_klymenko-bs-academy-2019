class PlacePage {

    get savedPlaceName () {return $('div.place-venue__place-name')};
    get placeCommentField () {return $('textarea.textarea')};
    get postCommentBtn () {return $('div.media-right button.is-primary')};
    get placeRate5Btn () {return $('//*[@id="app"]/div[2]/div[1]/div[3]/div[2]/div[2]/div[2]/form/div/section/div/div[5]/i')};
    get lastCommentText () {return $('//*[@id="app"]/div[2]/div[2]/div/div/div/div[3]/div[2]/div[1]/div/article/div/div[2]/p')};
    get checkInBtn () {return $('button.button.is-primary.checkin')};
    get saveBtn () {return $('button.button.is-success')};
    get saveDropDown () {return $('//*[@id="app"]/div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/div/a[2]')};

};

module.exports = PlacePage;