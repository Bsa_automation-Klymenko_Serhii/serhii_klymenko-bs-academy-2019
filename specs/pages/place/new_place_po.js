class NewPlacePage {

    get placeName () {return $('//*[@id="app"]/div[2]/div/div/section/div[1]/div[1]/div[2]/div/div/input')};
    get placeCity () {return $('//*[@id="app"]/div[2]/div/div/section/div[1]/div[3]/div[2]/div/div/div/div/div/div[1]/input')};
    get placeZip () {return $('//*[@id="app"]/div[2]/div/div/section/div[1]/div[4]/div[2]/div/div/input')};
    get placeAdress () {return $('//*[@id="app"]/div[2]/div/div/section/div[1]/div[5]/div[2]/div/div/input')};
    get placePhone () {return $('input[type="tel"]')};
    get placeSite () {return $('//*[@id="app"]/div[2]/div/div/section/div[1]/div[9]/div[2]/div/div/input')};
    get placeDesc () {return $('textarea.textarea')};
    get cityDropDown () {return $$('div.dropdown-content')[3]};
    get nextStepBtn () {return $$('div.buttons span.button.is-success')};
    get pictureUpload () {return $('input[type=file]')};
    get categorySelect () {return $('//*[@id="app"]/div[2]/div/div/section/div[4]/div[1]/div[1]/div/div/div/span/select')};
    get tagSelect () {return $('//*[@id="app"]/div[2]/div/div/section/div[4]/div[1]/div[2]/div/div/div/span/select')};
    get featureSelect () {return $$('label.switch')[0]};
    get imagePreview () {return $('img.image-preview')};
};

module.exports = NewPlacePage;